#pragma once
enum Number
{
	ONE=1, TWO,THREE
};

enum Symbol
{
	DIAMOND=1, SQUIGGLE, OVAL
};

enum Shading
{
	SOLID=1, STRIPED, OPEN
};

enum Color
{
	RED=1,GREEN,BLUE
};

class Deck
{
public:
	Deck();
	void getNameDeck() const;
	void PrintDeck();
	~Deck();
private:
	const char* d_name = "Francois";
	int m_number;
	int m_symbol;
	int m_shading;
	int m_color;

};

