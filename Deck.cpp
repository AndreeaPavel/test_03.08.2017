#include "Deck.h"
#include<iostream>

Deck::Deck()
{
	Deck *v_deck = new Deck[82];

	for (int i = 0; i < 81; i++)
	{
		for (int j = Number::ONE; j <= Number::THREE; j++)
			for (int k = Symbol::DIAMOND; k <= Symbol::OVAL; k++)
				for (int l = Shading::SOLID; l <= Shading::OPEN; l++)
					for (int m = Color::RED; m <= Color::BLUE; m++)
					{
						v_deck[i].m_number = j;
						v_deck[i].m_symbol = k;
						v_deck[i].m_shading = l;
						v_deck[i].m_color = m;
					}
	}
    delete[] v_deck;
}

void Deck::getNameDeck() const
{
	std::cout << d_name << std::endl;
}

void Deck::PrintDeck()
{
	for (int i = 0; i < 81; i++)
	{
		std::cout << "This card is ";

		switch (this->m_number)
		{
		case 1:
			std::cout << "ONE, ";
			break;
		case 2:
			std::cout << "TWO, ";
			break;
		case 3:
			std::cout << "THREE, ";
		}

		switch (this->m_symbol)
		{
		case 1:
			std::cout << "DIAMOND, ";
			break;
		case 2:
			std::cout << "SQUIGGLE, ";
			break;
		case 3:
			std::cout << "OVAL, ";
		}

		switch (this->m_shading)
		{
		case 1:
			std::cout << "SOLID, ";
			break;
		case 2:
			std::cout << "STRIPED, ";
			break;
		case 3:
			std::cout << "OPEN, ";
		}

		switch (this->m_color)
		{
		case 1:
			std::cout << "RED" << std::endl;
			break;
		case 2:
			std::cout << "GREEN"<<std::endl;
			break;
		case 3:
			std::cout << "BLUE"<<std::endl;
		}
	}
}

Deck::~Deck()
{
	delete[] d_name;
}
